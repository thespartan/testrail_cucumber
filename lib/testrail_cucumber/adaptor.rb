require 'testrail_cucumber/api_client'
require 'testrail_cucumber/testrail_client'
require 'pry'

module TestrailCucumber

  class Adaptor

    def initialize(
        enabled: false,
        test_suite: nil,
        url:,
        username:,
        password:,
        project_id:,
        suite_id:,
        close_run:
    )
      @close_run = close_run
      @enabled = enabled
      return unless @enabled
      if test_suite.nil?
        testrail_client = TestrailCucumber::APIClient.new(url)
        testrail_client.user = username
        testrail_client.password = password
        @test_suite = TestrailCucumber::TestRailClient.new(testrail_client).get_suite(
            project_id: project_id,
            suite_id: suite_id
        )
      else
        @test_suite = test_suite
      end
    end

    # Submits an scenario test results
    # If the test case exists, it will reuse the id, otherwise it will create a new Test Case in TestRails
    # @param scenario [Cucumber Scenario|Cucumber Scenario Outline] A test case scenario after execution
    def submit(scenario)
      return unless @enabled
      case scenario.class.name
        when 'Cucumber::RunningTestCase::ScenarioOutlineExample'
          test_results = resolve_from_scenario_outline(scenario)
        when 'Cucumber::Ast::OutlineTable::ExampleRow'
          test_results = resolve_from_scenario_outline(scenario)
        when 'Cucumber::RunningTestCase::Scenario'
          test_results = resolve_from_simple_scenario(scenario)
        when 'Cucumber::Ast::Scenario'
          test_results = resolve_from_simple_scenario(scenario)
      end
      submit_test_result(test_results)
    end

    def resolve_from_scenario_outline(scenario)
      {
          section_name: scenario.scenario_outline.feature.name.strip,
          test_name: "#{scenario.scenario_outline.name.strip} #{scenario.name.strip}",
          success: !scenario.failed?,
          comment: scenario.exception
      }
    end

    def resolve_from_simple_scenario(scenario)
      {
          section_name: scenario.feature.name.strip,
          test_name: scenario.name.strip,
          success: !scenario.failed?,
          comment: scenario.exception
      }
    end


    # This method initiates a test run against a project, and specified testsuite.
    # ruby functional test file (.rb) containing a range of rspec test cases.
    # Each rspec test case (in the ruby functional test file) will have a corresponding Test Case in TestRail.
    # These Test Rail test cases will belong to a test suite that has the title of the corresponding
    # ruby functional test file.
    def start_test_run
      return unless @enabled
      @test_run = @test_suite.start_test_run
    end

    # Checks to see if any of the tests in a particular test run have failed, if they have then the
    # it will leave the run opened. If there are no failed tests then it will call close the particular run.
    def end_test_run
      return if !@enabled || @test_run.nil?
      @test_run.submit_results
      @test_run.close unless @test_run.failure_count > 0 || @close_run == false
    end

    protected

    def submit_test_result(
        section_name:,
        test_name:,
        success:,
        comment:
    )
      @test_run.add_test_result(
          section_name: section_name,
          test_name: test_name,
          success: success,
          comment: comment
      )
    end

  end

end
