lib = File.expand_path("../lib", __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require "testrail_cucumber/version"

Gem::Specification.new do |s|
  s.name = "testrail_cucumber"
  s.version = TestrailCucumber::VERSION
  s.authors = ['The Spartan']
  s.email = ['carlos@spartan-testsolutions.co.uk']

  s.summary = %q{Cucumber integration with Testrail}
  s.description = %q{A gem that integrates Cucumber tests with Testrail using the Testrail API}
  s.homepage = 'https://github.com/thespartan1980'
  s.license = "MIT"
  s.files = `git ls-files`.split("\n").reject {|path| path =~ /\.gitignore$/} # s.require_paths = ["lib"]

  s.add_development_dependency 'bundler', '~> 1.16'
  s.add_development_dependency 'pry'
  s.add_development_dependency 'rake', '~> 12.3'
  s.add_development_dependency 'require_all'
  s.add_development_dependency 'rspec', '~> 3.8'
end
